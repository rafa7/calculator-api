namespace :setup do
  desc "Execute all setup"
  task :all => :environment do |t, args|
    begin
      puts "Setting up main database"
      Rake::Task["setup:init"].invoke
      puts ("\n"*2)
      puts "Setting up test database".yellow
      system "RAILS_ENV=test rake setup:init_test"
    rescue => exception
      puts "\nERROR in task: \n".red
      puts exception.backtrace
      puts exception.message.red
      puts ":-FailedTask"
      exit(1)
    end
  end

  desc"Reloads main database | Runs seeds"
  task :init => :environment do |t, args|
    print "Executing "
    print "setup:init ".yellow
    print "in: "
    puts Rails.env.yellow

    puts "This will reload the schema in existing database, are you sure? (Y/n)".red
    print "=> "
    answer = STDIN.gets.strip

    # Loads schema.rb
    if ["yes", "y"].include? answer.downcase
      begin
        starting = TimeHelper.get_time
        run_setup_init
        puts "\nSetup complete! in #{TimeHelper.get_elapsed_time(starting)} minutes".green
      rescue => exception
        puts "\nERROR in task: \n".red
        puts exception.backtrace
        puts exception.message.red
        puts ":-FailedTask"
        exit(1)
      end
    end
  end

  def run_setup_init_test
    unless Rails.env.test?
      puts "Environment warning".yellow
      fail(ArgumentError, "You are not in test environment, run with 'RAILS_ENV=test'")
    end

    reload_schema
    sql_init_database
  end

  def sql_init_database
    Rake::Task["sql:run_sql"].invoke
  end

  def reload_schema
    puts ("=" * 30).yellow
    puts "Re-loading schema in existing database".red
    system "DISABLE_DATABASE_ENVIRONMENT_CHECK=1 rake db:schema:load"
    puts "Schema re-loaded".green
    puts ("=" * 30).yellow
  end
end