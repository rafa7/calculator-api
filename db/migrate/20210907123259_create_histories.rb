class CreateHistories < ActiveRecord::Migration[6.1]
  def change
    create_table :histories do |t|
      t.text :arguments
      t.string :result

      t.timestamps
    end
  end
end
