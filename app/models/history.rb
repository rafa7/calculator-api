class History < ApplicationRecord

  def self.register(result, arguments)
    history = History.new(result: result.to_s, arguments: arguments)
    history.save!
  end
end
