class Api::V1::HistoriesController < ApplicationController

  def index
    page_number =  params[:page].present? ? params[:page].to_i : 1
    per_page = params[:per_page].present? ? params[:per_page].to_i : 10

    histories = History.paginate(page: page_number, per_page: per_page)
    
    render status: :ok,
      json: {
        status: :ok,
        meta: {
          total_pages: histories.total_pages,
          page_number: page_number,
          max_per_page: per_page,
          total_resources: histories.total_entries
        },
        data: histories
      }
  end
end  