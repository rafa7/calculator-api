class Api::V1::CalculateController < ApplicationController
  def calc
    begin
      result = 0
      arguments = []
      if calculate_params[:filters].present?
        calculate_params[:filters].each do |object|
          arguments << object["value"]
        end
        
        arguments.each do | value |
          if !is_n?(value)
            History.register("ERROR", arguments.join(" - "))
            raise "Invalid values exist"
          end
          result += value.to_f
        end
      end

      History.register(result, arguments.join(" - "))

      render status: :ok,
        json: {
          status: :ok,
          data: result
        }
    rescue => exception
      render status: :unprocessable_entity,
        json: {
          error: exception
        }
    end
  end

  private


  def is_n?(value)
    /\A-?(?:\d+(?:\.\d*)?|\.\d+)\z/ === value
  end

  def calculate_params
    params.require(:calculate)
    .permit(
      filters: [:value]
    )
  end

end