require 'rails_helper'

RSpec.describe 'Calculate', type: :request do
  describe 'POST /api/v1/calc' do

    let(:calculate_params) {
      {
        calculate: {
          filters: [
            {
              value: "error"
            },
            {
              value: "2"
            }
          ]
        }
      } 
    }

    before do
      post '/api/v1/calc',
      params: calculate_params
    end

    it 'returns unprocessable_entity' do
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end

  describe 'POST /api/v1/calc' do

    let(:calculate_params) {
      {
        calculate: {
          filters: [
            {
              value: "3"
            },
            {
              value: "2"
            }
          ]
        }
      } 
    }

    before do
      post '/api/v1/calc',
      params: calculate_params
    end

    it 'returns ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns 5.0' do
      expect(JSON.parse(response.body)["data"]).to equal(5.to_f)
    end
  end

end