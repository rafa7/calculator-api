require 'rails_helper'

RSpec.describe 'Histories', type: :request do
  describe 'GET /api/v1/histories' do
    let(:data) { create(:history) }

    before do
      get '/api/v1/histories'
    end

    it 'returns ok' do
      expect(response).to have_http_status(:ok)
    end
  end
end